const fs = require("fs");
const csv = require("csvtojson");
const { pipeline } = require("stream");

const inputFile = "./csv/nodejs-hw1-ex1.csv";
const outputFile = "./csv/nodejs-hw1-ex2.txt";

const readStream = fs.createReadStream(inputFile);
const writeStream = fs.createWriteStream(outputFile);

pipeline(
  readStream,
  csv({ ignoreColumns: /(amount)/ }).preFileLine((fileLineString, lineIdx) => {
    return lineIdx === 0 ? fileLineString.toLowerCase() : fileLineString;
  }),
  writeStream,
  (err) => {
    if (err) {
      console.error("Pipeline failed.", err);
    }
  }
);
