import { createReadStream, createWriteStream } from "fs";
import csv from "csvtojson";
import { pipeline } from "stream";

const inputFile = "./csv/nodejs-hw1-ex1.csv";
const outputFile = "./csv/nodejs-hw1-ex2.txt";

const readStream = createReadStream(inputFile);
const writeStream = createWriteStream(outputFile);

pipeline(
  readStream,
  csv({ ignoreColumns: /(amount)/ }).preFileLine((fileLineString, lineIdx) => {
    return lineIdx === 0 ? fileLineString.toLowerCase() : fileLineString;
  }),
  writeStream,
  (err) => {
    if (err) {
      console.error("Pipeline failed.", err);
    }
  }
);
